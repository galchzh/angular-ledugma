import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() data:any;
  
  key:string;
  name:string;
  price:string;
  stock:boolean;


  showTheButton = false;
  deleteBut = false;

  
  cancel()
  {
   this.showTheButton = false;
   this.deleteBut = false;
  }

  checkChange()
  {
    this.itemsService.updateItem(this.key,this.stock);
  }

  showButton()
  {
    this.showTheButton = true;
  }

  hideButton()
  {
    this.showTheButton = false;
  }

  show2Buttons()
  {
    this.deleteBut = true;
  }

  deleteItem()
  {
    this.itemsService.deleteItem(this.key);
  }


  constructor(private itemsService: ItemsService) { }

  ngOnInit() {
    this.key = this.data.key;
    this.name = this.data.name;
    this.price = this.data.price;
    this.stock = this.data.stock;
  }

}
