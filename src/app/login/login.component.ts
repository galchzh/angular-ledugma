import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  password='';
  email='';
  code = '';
  message = '';
  hide = true;
  require = true;
  required = '';
  
  login()
  {
    if(this.email == '' || this.password == '')
    {
      this.required = "this input is required";
      this.require = false;
    }
    if (this.require)
    {
      this.authService.login(this.email,this.password)
      .then(value =>
      {
        this.router.navigate(['/items']);
      })
      .catch(err =>
      {
        this.code = err.code;
        this.message = err.message;
      })
    }
  }

  

  constructor(private authService: AuthService, private router:Router) { }

  ngOnInit() {
  }

}
