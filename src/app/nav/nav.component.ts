import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toLogin() 
  {
    this.router.navigate(['/login']);
  }

  toItems()
  {
    this.router.navigate(['/items']);
  }

  toLogout()
  {
    this.authService.logout()
    .then(value => {
      this.router.navigate(['/login']);
    });
   
  }

  constructor(private router:Router, public authService: AuthService) { }

  ngOnInit() {
  }

}
