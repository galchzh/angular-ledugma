import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items=[];

  constructor(public authService:AuthService, private db:AngularFireDatabase) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe(
        items =>
        {
          this.items = [];
          items.forEach(
            item => {
              let y = item.payload.toJSON();
              y["key"] = item.key;
              this.items.push(y);
            }
          )
          console.log(this.items);
        }
        
      )
    })
  }

}
