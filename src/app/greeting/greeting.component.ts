import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'greeting',
  templateUrl: './greeting.component.html',
  styleUrls: ['./greeting.component.css']
})
export class GreetingComponent implements OnInit {

  email:string;

  constructor(public authService:AuthService) { }

  ngOnInit() {
  
    this.authService.user.subscribe(user =>
    {
      if(user != null)
      {
        this.email = user.email;
      }
    })
 
}

}
